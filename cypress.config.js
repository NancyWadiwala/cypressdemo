const { defineConfig } = require("cypress");
const {downloadFile} = require('cypress-downloadfile/lib/addPlugin'); 

module.exports = defineConfig(
{
  e2e:
   {
    
   setupNodeEvents(on, config) 
   {
      // implement node event listeners here
    
    on('task', {downloadFile})
    //"URL"; "http://136.233.176.218:7022/"

    }
  }
})
{
  "watchForFilechanges"; true
  "reporter";"mochawesome"
  "reporterOptions";
  {
      "charts";true,
      "overwrite";false,
      "html";false,
      "json";true,
      "reportDir";"cypress/reports"
  }
}
