export class LoginPage
{
    loginPage_username = '#UserName'
    loginPage_password = '#Password'
    loginPage_loginButton = '#btnSubmit'

navigate(url)
{
    cy.visit(url)
    cy.wait(500)
}

enterUsername(username)
{
    cy.get(this.loginPage_username).type(username)
    cy.wait(500)
}

emptyUsername()
{
    cy.get(this.loginPage_username).should('be.empty')
    cy.wait(500)
}

enterPassword(password)
{
    cy.get(this.loginPage_password).type(password)
    cy.wait(500)
}

emptyPassword()
{
    cy.get(this.loginPage_password).should('be.empty')
    cy.wait(500)
}

clickLogin()
{
    cy.get(this.loginPage_loginButton).click()
    cy.wait(500)
}
}