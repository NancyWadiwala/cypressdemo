
export class Settingpage
//export class LoginPage
{
    loginPage_username = '#UserName'
    loginPage_password = '#Password'
    loginPage_loginButton = '#btnSubmit'
    userProfile_click = '[href="/Profile/Detail"]'
    userupdateProfile_click = '.edit-profile > .btn'
    settingPage_username = '#UserName'
    settingPage_designaion = '#designation'
    settingPage_phoneno = '#phoneNo'
    settingPage_clicksubmit = '#btnSubmit'
    userChangePassword_click = '[href="/Profile/ChangePassword"]'
    changepassword_clicksubmit = '#btnSubmit'
    changepassword_currentPassword = '#CurrentPassword'
    changepassword_newPassword = 'NewPassword'
    changepassword_confirmPassword = 'ConfirmPassword'
    


navigate(url)
{
    cy.visit(url)
    cy.wait(500)
}

enterUsername(username)
{
    cy.get(this.loginPage_username).type(username)
    cy.wait(500)
}

emptyUsername()
{
    cy.get(this.loginPage_username).should('be.empty')
    cy.wait(500)
}

enterPassword(password)
{
    cy.get(this.loginPage_password).type(password)
    cy.wait(500)
}

emptyPassword()
{
    cy.get(this.loginPage_password).should('be.empty')
    cy.wait(500)
}

clickLogin()
{
    cy.get(this.loginPage_loginButton).click()
    cy.wait(500)
}

userProfileclick()
{
    cy.contains('N').trigger('mouseover')
    cy.wait(500)
    cy.get(this.userProfile_click).click({force:true})
    cy.wait(500)
    
}

userUpdateProfileclick()
{
    cy.get(this.userupdateProfile_click).click()
    cy.wait(500)


}
enterUsername_update(username1)
{   
    cy.get(this.settingPage_username).clear().type(username1)
    cy.wait(500)
}

emptyUsername1()
{   
    cy.get(this.settingPage_username).clear().should('be.empty')
    cy.wait(500)
}

enterDesignation(designation)
{
    cy.get(this.settingPage_designaion).clear().type(designation)
}

emptyDesignation()
{
    cy.get(this.settingPage_designaion).clear().should('be.empty')
}

enterPhoneno(phoneno)
{
    cy.get(this.settingPage_phoneno).clear().type(phoneno)

}

emptyPhoneno()
{
    cy.get(this.settingPage_phoneno).clear().should('be.empty')
}

userupdateclickSubmit()
{
    cy.get(this.settingPage_clicksubmit).click()
}

userChangepasswordclick()
{
    //cy.contains('N').trigger('mouseover')
    //cy.wait(500)
    cy.get(this.userChangePassword_click).click()
    cy.wait(500)
}

currentPassword(currentpassword)
{
    cy.get(this.changepassword_currentPassword).clear().type(currentpassword)
}
    
newPassword(newpassword)   
{
    cy.get(this.changepassword_newPassword).clear().type(newpassword)
}

confirmPassword(confirmpassword)
{
    cy.get(this.changepassword_confirmPassword).clear().type(confirmpassword)
}

changePasswordsubmitclick()
{
    cy.get(this.changepassword_clicksubmit).click({force:true})
    cy.wait(500)
}


}